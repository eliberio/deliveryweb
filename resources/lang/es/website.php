<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
	*/
	'Location' => 'Ubicación',
	'Please enter your location or click here to open map' => 'Clic aquí para abrir el mapa.',
    'Guest Checkout' => 'Pago de invitado',
  'Ops! Product is available in stock But Not Active For Sale. Please contact to the admin' => 'Ops! El producto está disponible en stock, pero no está activo para la venta. Por favor, pónte en contacto con nosotros',
  'Ops! you have already bought full stock. If you like this product and want more contact to the admin' => 'Ops! Ya has comprado este Stock. Si te gusta este producto y quieres más contacta con nosotros',
  'Ops! The amount you can add more is' => 'Ops! La cantidad que puede agregar más es',
  'If you do not want to apply this coupon just click cross button of this alert.' => 'i no deseas aplicar este cupón, simplemente haz clic en el botón cruzado de esta alerta.',
  'Your address has been deteled successfully' => 'Tu dirección ha sido eliminada correctamente',
  'Your address has been changed successfully' => 'Tu dirección ha sido cambiada correctamente',
  'payment method' => 'método de pago',
  'DOWNLOAD OUR APPS' => 'DESCARGA NUESTRA APP',
  'SUBSCRIBE NOW FOR THE LATEST NEWS AND GET 10% DISCOUNT ON SPECIAL ITEMS' => 'SUSCRÍBETE AHORA PARA LAS ÚLTIMAS NOTICIAS Y OBTEN UN 10% DE DESCUENTO EN ARTÍCULOS ESPECIALES',
  'Hotline' => 'Liquidación',
  'Call Us Now' => 'Llámanos',
  'Product Is Ready To Comapre!' => '¡El producto está listo para comparar!',
  'Product Quantity Exceeding From Current Stock Please Enter A Valid Quantity!' => 'Cantidad del producto superior al stock actual ¡Introduce una cantidad válida!',
	'bannerLabel1'=>'Envío Gratis',
	'Compare' => 'Comparar',
	'Product is liked' => 'El producto se ha añadido a la lista de deseos',
	'Product is disliked' => 'El producto se ha eliminado de la lista de deseos',
	'Please Login First!' => 'Por favor Accede primero',
	'Product added successfully!' => '¡Producto añadido con éxito!',
	'Choose Any Category' => 'Categorías',
	'bannerLabel1Text'=>'En pedidos superiores a 30€',

	'bannerLabel2'=>'Cupones Descuento',

	'bannerLabel2Text'=>'Ahorro en tus Compras',

	'bannerLabel3'=>'Soporte 24/7',

	'hotline'=>'Incidencias',

	'bannerLabel4'=>'Pago seguro',

	'bannerLabel4Text'=>'Pago en Línea protegido',

	'TopSales'=>'Top Ventas',

	'Special'=>'Especial',

	'MostLiked'=>'más gustado',

	'New'=>'Nuevo',

	'Likes'=>'Likes',

	'Welcome to our Store'=>'Bienvenid@ a App´u',

	'Orders'=>'Pedidos',

	'All Categories'=>'Todas las categoríass',

	'Search entire store here'=>'Buscar Productos',

	'Profile'=>'Perfil',

	'Logout'=>'Salir',

  'View' => 'Ver',

  'Login/Register'=>'Acceso/Registro',

	'item(s)'=>'Productos(s)',

	'Quantity'=>'Cantidad',

	'Total Items'=>'Total Productos',

	'Total Price'=>'Precio total',

	'View Cart'=>'Ver carrito',

	'Checkout'=>'Pago',

	'Menu'=>'Menu',

	'My Cart'=>'Carrito',
	'MyCart'=>'Carrito',

	'footer text'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel nulla eros. Sed dui magna, faucibus et enim vel, auctor dapibus ante. Nullam in sem ultrices, efficitur arcu quis, fermentum mi.',

  'RECENT POST' => 'PUBLICACIÓN RECIENTE',

  'Helpful Links'=>'Enlaces Útiles',

	'Terms'=>'Condiciones',

	'Privacy'=>'Privacidad',

	'Discount'=>'Descuento',

	'About Us'=>'Sobre App´u',

	'Contact Us'=>'Contacto',

	'Terms & Condtions'=>'Términos y condiciones',

	'Wishlist'=>'Lista de deseos',

	'Shopping Cart'=>'Carro de compras',

	'Privacy Policy'=>'Política de privacidad',

	'Phone'=>'Teléfono',

	'Email'=>'Email',

	'Copy Rights'=>date('Y',time()).' Supermercado App´u',


	'View All News'=>'Ver todas las noticias',

	'Readmore'=>'Leer mas',

	'Weekly'=>'Semanal',

	'banner saving text'=>'Ahorra hasta un 50% de descuento en todos los artículos de la tienda esta semana.',

	'Shop now'=>'Compra ahora',

	'Sale'=>'Venta',

	'Top Selling of the Week'=>'Los más vendidos de la semana',

	'View Detail'=>'Ver Detalle',

	'Newest Products'=>'Nuevos Productos',

	'View All'=>'Ver Todo',

	'Special'=>'Especial',

	'items'=>'Productos',
	'item'=>'Producto',

	'SubTotal'=>'Sub Total',

	'Couponisappliedsuccessfully'=>'¡Cupón aplicado correctamente!',

	'You are not allowed to use this coupon'=>'No está permitido usar este cupón.',

	'This coupon has been reached to its maximum usage limit'=>'Se ha alcanzado este cupón hasta su límite de uso máximo.',

	'coupon is used limit'=>'El cupón se ha utilizado hasta su límite',

	'Coupon amount limit is low than minimum price'=>'El límite de la cantidad del cupón es inferior al precio mínimo.',

	'Coupon amount limit is exceeded than maximum price'=>'Se excede el límite de cantidad del precio máximo.',

	'Coupon cannot be applied this product is in sale'=>'No se puede aplicar el cupón este producto está en venta.',

	'Coupon amount is greater than total price'=>'La cantidad del cupón es mayor que el precio total.',

	'Coupon amount is greater than product price'=>'La cantidad del cupón es mayor que el precio del producto.',

	'Coupon does not exist'=>'El cupón no existe o el código de cupón no es válido.',

	'Please enter a valid coupon code'=>'Por favor introduce un código de cupón válido',

	'Coupon is already applied'=>'El cupón ya está aplicado',

	'Cart item has been deleted successfully'=>'¡El artículo del carrito se ha eliminado con éxito!',

	'Coupon has been removed successfully'=>'¡El cupón se ha eliminado con éxito!',

	'Cart has been updated successfully'=>'¡El carrito se ha actualizado correctamente!',

	'Coupon can not be apllied to empty cart'=>'El cupón no se puede aplicar al carrito vacío.',

	'cartEmptyText'=>'No tienes artículos en su carro de compra.<br>Click <a href="shop">aquí</a> para seguir comprando',

	'Home'=>'Inicio',

	'Shopping cart'=>'Carro de compra',

	'Poducts'=>'Poductos',

	'Detail'=>'Detalle',

	'Quantity'=>'Cantidad',

	'Unit Price'=>'Precio unitario',

	'Item Total'=>'Total',

	'Special'=>'Especial',

	'Coupon Code'=>'Código Cupón',

	'Checkout'=>'Pago',

	'Update Cart'=>'Actualizar carrito',

	'Back To Shopping'=>'SEGUIR COMPRANDO',

	'Total'=>'Total',

	'Discount(Coupon)'=>'Descuento (Cupón)',

	'Order Summary'=>'Resumen de pedido',

	'Subtotal'=>'Subtotal',

	'Apply'=>'Aplicar',

	'Please enter your first name'=>'Introduce tu nombre',

	'Please enter your last name'=>'Introduce tu apellido',

	'Login'=>'Acceso',

	'Create an Account'=>'Crear una Cuenta',

	'Personal Information'=>'Informacion Personal',

	'Error'=>'Error',
	'User'=>'Usuario',

	'Success'=>'Correcto',
	'Enter Your Email or Username' =>'Introduce tu Correo electrónico o Usuario',

	'First Name'=>'Nombre',

	'Last Name'=>'Apellidos',

	'Email Adrress'=>'Email',

	'Please enter your valid email address'=>'Introduce una dirección de correo electrónico válida',

	'Gender'=>'Género',

	'Please select your gender'=>'Selecciona tu género',

	'Picture'=>'magen',

	'Password'=>'Contraseña',

	'Please enter your password'=>'Introduce tu Contraseña',

	'Confirm Password'=>'Confirmar Contraseña',

	'Please re-enter your password'=>'Confirma tu contraseña.',

	'Password does not match the confirm password'=>'La contraseña no coincide con la contraseña de confirmación.',

	'Creating an account means you are okay with our'=>'Crear una cuenta significa que está de acuerdo con nuestras',

	'Terms and Services'=>'Condiciones de Servicio',

	'Privacy Policy'=>'Política de privacidad',
	'SPECIAL DEALS' => 'OFERTAS ESPECIALES',

	'Refund Policy'=>'Politica de reembolso',

	'Please accept our terms and conditions'=>'Debes aceptar nuestros términos y condiciones',

	'Sign Up'=>'Regístro',

	'Lorem ipsum dolor sit amet, consectetur adipiscing elit'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',

	'Duis at nisl luctus, malesuada diam non, mattis odio'=>'Duis at nisl luctus, malesuada diam non, mattis odio.',

	'Fusce porta neque at enim consequat, in vulputate tellus faucibus'=>'Fusce porta neque at enim consequat, in vulputate tellus faucibus.',

	'Pellentesque suscipit tortor id dui accumsan varius'=>'Pellentesque suscipit tortor id dui accumsan varius.',

	'Sed interdum purus imperdiet tortor imperdiet, et ultricies leo gravida'=>'Sed interdum purus imperdiet tortor imperdiet, et ultricies leo gravida.',

	'Aliquam pharetra urna vel nulla egestas, non laoreet mauris mollis'=>'Aliquam pharetra urna vel nulla egestas, non laoreet mauris mollis.',

	'Integer sed velit sit amet quam pharetra ullamcorper'=>'Integer sed velit sit amet quam pharetra ullamcorper.',

	'Proin eget nulla accumsan, finibus lacus aliquam, tincidunt turpis'=>'Proin eget nulla accumsan, finibus lacus aliquam, tincidunt turpis.',

	'Nam at orci tempor, mollis mi ornare, accumsan risus'=>'Nam at orci tempor, mollis mi ornare, accumsan risus.',

	'Cras vel ante vel augue convallis posuere'=>'Cras vel ante vel augue convallis posuere.',

	'Ut quis dolor accumsan, viverra neque nec, blandit leo'=>'Ut quis dolor accumsan, viverra neque nec, blandit leo.',

	'Checkout'=>'Pago',

	'Shopping cart'=>'Carro de compra',

	'Billing Address'=>'Dirección de envío',



	'Payment Methods'=>'Métodos de pago',

	'Order Detail'=>'Detalle del pedido',

	'Company'=>'Empresa',

	'Address'=>'Dirección',

	'Country'=>'País',

	'State'=>'Provincia',

	'City'=>'Ciudad',

	'Zip/Postal Code'=>'Código Postal',

	'Continue'=>'Continuar',

	'Please enter your company name'=>'Introduce el nombre de tu empresa.',

	'Please enter your address'=>'Introduce tu dirección.',

	'Please select your country'=>'Selecciona tu País.',

	'Please select your state'=>'Selecciona tu Provincia.',

	'Please enter your city'=>'Selecciona tu Ciudad.',

	'Please enter your Zip/Postal Code'=>'Selecciona tu Código Postal.',

	'Update'=>'Actualizar',

	'Add Address'=>'Añadir dirección',

	'Your address has been deteled successfully'=>'Tu dirección ha sido eliminada con éxito!',

	'Your address has been chnaged successfully'=>'¡Tu dirección ha sido cambiada con éxito!',

	'Default'=>'Defecto',

	'Address Info'=>'Información de dirección',

	'Action'=>'Acción',

	'Your address has been updated successfully'=>'Tu dirección ha sido actualizada con éxito!',

	'Same shipping and billing address'=>'Misma dirección de envío y facturación.',

	'Select Country'=>'Seleccionar País',

	'Select State'=>'Seleccionar Provincia',

	'Other'=>'Otro',

	'Shipping Methods'=>'Métodos de envío',

	'Confirm Order'=>'Confirmar Pedido',

	'Order Now'=>'Hacer Pedido',

	'Please select a prefered shipping method to use on this order'=>'Te enviamos tu Compra mediante:',

	'My Orders'=>'Mis Pedidos',

	'No order is placed yet'=>'No se ha realizado ningún pedido todavía.',

	'Order ID'=>'Pedido ID',

	'Quantity'=>'Cantidad',

	'Order Date'=>'Fecha de Pedido',

	'Price'=>'Precio',

	'View Order'=>'Ver Pedido',

	'Status'=>'Estado',

	'Order information'=>'Información del Pedido',

	'Please select a prefered shipping method to use on this order'=>'Te enviamos tu Compra mediante:',

	'Customer Detail'=>'Detalle del cliente',

	'Admin Comments'=>'Comentarios del administrador',

	'Order Comments'=>'Comentarios del pedido',

	'Comments'=>'Comentarios',

	'Shipping Cost'=>'Precio de Envío',

	'Tax'=>'Impuesto',

	'Payment Method'=>'Método de pago',

	'Shipping Method'=>'Método de envío',

	'Payment/Shipping Method'=>'Pago/Método de envío',

	'Billing Detail'=>'Detalle de facturación',

	'Shipping Detail'=>'Detalle de envío',

	'Pay'=>'Pagar',

	'Your location does not support this'=>'Tu ubicación no es compatible ',

	'Please select your shipping method'=>'Selecciona tu método de envío.',

	'Please select a prefered payment method to use on this order'=>'Selecciona un método de pago preferido para usar en este pedido.',

	'Please select your payment method'=>'Selecciona tu forma de pago.',

	'BrainTree Payment'=>'Pago de BrainTree',

	'Card number'=>'Número de tarjeta',

	'Expiration'=>'Vencimiento',

	'CVC'=>'CVC',

	'Payment successful'=>'Pago realizado',

	'Thanks You Your payment has been processed successfully'=>'¡Gracias! Tu pago ha sido procesado correctamente.',

	'Edit Cart'=>'Editar carro',

	'Full Name'=>'Nombre Completo',

	'Email'=>'Email',

	'Message'=>'Mensaje',

	'Our Address'=>'Nuestra dirección',

	'contact us message'=>'Gracias por contactarnos. Nos pondremos en contacto contigo dentro de los tres días hábiles.',

	'Subject'=>'Asunto',

	'Send'=>'Enviar',

	'News Detail'=>'Detalle de noticias',
	'Date'=>'Fecha',

	'News'=>'Noticias',

	'All News'=>'Todas las Noticias',

	'Sort'=>'Ordenar',

	'Shop'=>'Tienda',

	'Newest'=>'Lo más nuevo',

	'Price: High To Low'=>'Precio más Alto',

	'Price: Low To High'=>'Precio más Barato',

	'Top Seller'=>'Mejor Vendedor',

	'Special Products'=>'Ofertas',

	'Most Liked'=>'Más Gustado',

	'Display'=>'Mostrar',

	'Limit'=>'Límite',

	'No record found'=>'¡Ningún registro fue encontrado!',

	'All products are loaded.'=>'Todos los productos están cargados.',

	'All news are loaded'=>'Todas las noticias están cargadas.',

	'Load More'=>'Mostrar más',

	'Oldest'=>'Más antiguo',

	'News Categories'=>'Categorías de noticias',

	'A - Z'=>'A - Z',

	'Z - A'=>'Z - A',

	'Edit Address'=>'Editar dirección',

	'Add Address'=>'Añadir dirección',

	'All products are loaded'=>'Todos los productos están cargados.',

	'Related Products'=>'Productos Relacionados',

	'Products Description'=>'Descripción de productos',

	'Order(s)'=>'Pedido(s)',

	'Login Or Create An Account'=>'Acceder o Crear una cuenta',

	'New Customers'=>'Nuevos clientes',

	'login page text for customer'=>'Al crear una cuenta en nuestra tienda, podrás pasar por el proceso de pago más rápido, almacenar múltiples direcciones de envío, ver y rastrear tus pedidos en tu cuenta y más.',

	'Dont have account'=>'¿No tienes cuenta?',

	'or Sign in with'=>'o Inicia sesión con',

	'Login with Facebook'=>'Acceder con Facebook',

	'Login with Google'=>'Acceder con Google',

	'Registered Customers'=>'Clientes Registrados',

	'If you have an account with us, please log in'=>'Si tienes una cuenta con nosotros, inicia sesión',

	'This field is required'=>'Este campo es requerido.',

	'Forgot Password'=>'¿Se te olvidó tu contraseña?',

	'Please enter your password and should be at least 6 characters long'=>'Introduce tu contraseña, debe tener al menos 6 caracteres de longitud.',

	'New Password'=>'Nueva contraseña',

	'Current Password'=>'Contraseña actual',

	'Please enter current password'=>'Introduce la contraseña actual.',

	'Please enter your valid phone number'=>'Introduce tu número de teléfono.',

	'Phone Number'=>'Número de teléfono',

	'Please enter your date of birth'=>'Por favor, introduce tu fecha de nacimiento.',

	'Date of Birth'=>'Fecha de Nacimiento',

	'Female'=>'Mujer',

	'Male'=>'Hombre',

	'Gender'=>'Género',

	'Ecommerce'=>'App´u',

	'Featured'=>'Destacado',

	'Filters'=>'Filtros',

	'Apply'=>'Aplicar',

	'Reset'=>'Reiniciar',

	'Subscribe'=>'Suscribir',

	'Shipping Address'=>'Datos personales',

	'Returned all products'=>'Devuelto todos los productos.',

	'Search results empty'=>'Resultados de búsqueda vacíos.',

	'Categories'=>'Categorías',

	'Product Detail'=>'Detalle del producto',

	'Returned all filters successfully'=>'Filtros eliminados.',

	'Filter is empty for this category'=>'El filtro está vacío para esta categoría.',

	'Select Zone'=>'Seleccionar zona',

	'select Country'=>'Seleccionar País',

	'All Products'=>'Todos los productos',

	'Product is added'=>'Producto añadido!',

	'Special Products of the Week'=>'Productos en Oferta de la semana',

	'Forgot Password'=>'Se te olvidó tu contraseña',

	'Please Enter your email to recover your password'=>'Introduce tu correo electrónico para recuperar tu contraseña.',

	'Qty'=>'Cantidad',

	'You have no items in your shopping cart'=>'No tienes artículos en tu carrito de compras.',

	'Password has been sent to your email address'=>'La contraseña ha sido enviada a tu dirección de correo electrónico.',

	'Email address does not exist'=>'La dirección de correo electrónico no existe.',

	'Add to Cart'=>'Añadir',

	'Added'=>'Añadido',

	'Subscribe for Newsletter'=>'Suscríbete al boletín',

	'Your email address here'=>'Tu dirección de correo electrónico aquí ..',


	'About Store'=>'Sobre la tienda',

	'Our Services'=>'Nuestros servicios',

	'Information'=>'Información',

	'Popular Categories'=>'Categorías Populares',

	'Error while placing order'=>'Error al hacer el pedido.',

	'Payment has been processed successfully'=>'¡El pago ha sido procesado correctamente!',

	'Your order has been placed'=>'Tu pedido ha sido colocado',

	'Email or password is incorrect'=>'¡Correo electrónico o la contraseña son incorrectos!',

	'something is wrong'=>'¡algo está mal!',

	'Email already exist'=>'¡Ya existe este correo electrónico!',


	'ApplyCoupon'=>'Aplicar cupón',

	'proceedToCheckout'=>'Pasar por Caja',

	'orderNotes'=>'Pedidos',

	'Previous'=>'Anterior',
	'Next'=>'Siguiente',

	'Out of Stock'=>'Fuera de Stock',

	'In stock'=>'En Stock',

	'Low in Stock'=>'Bajo en Stock',

	'Edit'=>'Editar',

	'Remove Item'=>'Eliminar Artículo',

	'per page'=>'por página',

	'orderNotesandSummary'=>'Notas de pedido y resumen',

	'Signup'=>'Regístro',

	'myProfile'=>'Mi Perfil',

	'orderID'=>'Pedido ID',

	'orderStatus'=>'Estado del Pedido',

	'Please enter your name'=>'Por favor, escriba su nombre.',

	'Please enter your message'=>'Por favor escribe tu mensaje.',

	'Profile has been updated successfully'=>'¡El perfil ha sido actualizado con éxito!',

	'Change Password'=>'Cambiar la Contraseña',

	'infoPages'=>'Páginas de información',

	'homePages'=>'Páginas de inicio',

	'homePage1'=>'App´u 1',
	'homePage2'=>'App´u 2',
	'homePage3'=>'App´u 3',
	'homePage4'=>'App´u 4',
	'homePage5'=>'App´u 5',

	'collection'=>'Colección',

	'hot'=>'Oferta',

	'new in Stores'=>'Nuevo en tiendas',

	'categroies'=>'Categorías',

	'Call Us Free'=>'Llámenos Gratis',

	'Featured News'=>'Noticias destacadas',

	'Upload Profile Photo'=>'Subir foto de perfil',

	'Choose...'=>'Elegir...',

	'Showing'=>'Mostrando',

	'of'=>'de',

	'results'=>'resultados.',

	'Password has been updated successfully'=>'¡La contraseña ha sido actualizada con éxito!',

	'Record not found'=>'¡Registro no encontrado!',
	'contact us title'=>'Contacte con nosotros',
	'Follow Us'=>'Síguenos',

	'Shipping addresses are not added yet'=>'Las direcciones de envío aún no se han añadido.',

	'product is not added to your wish list'=>'El producto no se añadió a tu lista de deseos.',

	'Search result for'=>'Resultado de búsqueda para',

	'item found'=>'Artículo encontrado.',
	'cancel'=>'cancelar',

	'Please write notes of your order'=>'Por favor escribe notas a tu pedido',
	'Coupon Applied'=>'Cupón Aplicado',

	//new
	'Welcome'=>'Bienvenid@',
	'Welcome Guest!'=>'Bienvenid@ Invitad@!',
	'Worldwide Express Plus'=>'Worldwide Express Plus',
	'Standard'=>'Normal',
	'Worldwide Expedited'=>'Acelerasdo en todo el mundo',
	'Worldwide Express'=>'Entrega Internacional',
	'2nd Day Air A.M.'=>'Segundo día por la mañana',
	'Next Day Air Early A.M.'=>'Siguiente día por la mañana',
	'Next Day Air Saver'=>'Siguiente día',
	'3 Day Select'=>'Selección de 3 días',
	'Ground'=>'Básico',
	'2nd Day Air'=>'Segundo día',
	'Next Day Air'=>'Siguiente día',
	'My Account'=>'Mi Cuenta',
	'Info Pages'=>'Páginas de información',
	'Dummy Text'=>'',

	'Product is unliked'=>'El producto no es del agrado.',
	'Please login first to like this product'=>'Inicia sesión primero para añadir este producto a la lista de deseos.',
	'Password has been updated successfully'=>'La Contraseña ha sido actualizada correctamente.',
	'Empty Site Name'=>'Nombre de sitio vacío',
	'Instamojo Payment'=>'Pago Instamojo',
	'External Link'=>'Enlace externo',
	'Flash Sale'=>'Venta Express',
	'cancel order'=>'Cancel Pedido',
	'cancelled'=>'Cancelado',
	'Are you sure you want to cancel this order?'=>'¿Estás seguro de que deseas cancelar este pedido?',
	'Flash Sale'=>'Venta Express',
	'UP COMMING'=>'PRÓXIMAMENTE',
	'Min Order Limit:'=>'Límite mínimo de pedido:',
	'Return'=>'Devolver',
	'Are you sure you want to return this order?'=>'¿Seguro que quieres devolver este pedido?',
	'return order'=>'Orden de devolución',
	'Avail free shpping on'=>'Disponible envío gratis en',
	'Coupon does not belongs to this product'=>'El cupón no pertenece a este producto.',
	'You have successfully subscribed.'=>'Te has suscrito correctamente.',
	'You have already subscribed.'=>'Ya te has suscrito.',
	'Some problem occurred, please try again.'=>'Se produjo algún problema, por favor inténtalo nuevamente.',
	'Please enter your email address'=>'Por favor, introduce tu dirección de correo electrónico.',
	'If you do note want to apply this coupon just click cross button of this alert.'=>'Si deseas aplicar este cupón, simplemente haz clic en el botón cruzado de esta alerta.',
	'You Are Not Allowed With These Credentials!'=>'Introduce datos de tu cuenta válidos.',
	'Removed Successfully'=>'Eliminado con éxito',
	'error'=>'Error',	
	'success'=>'Realizado',
	
	///// new labels /////
	'Search Products'=>'Buscar Productos',
	'Currency'=>'Moneda',
	'Footer text 1'=>'El primer Supermercado solo Online.',
	'Quick Links'=>'Enlaces Rápidos',
	'Personalization'=>'Personalización',
	'Instagram Feed'=>'Cargar en Instagram',
	'Enter Email here'=>'Introduce el correo electrónico aquí',
	'Submit'=>'Enviar',
	'Newsletter'=>'Boletín App´u',
	'DOWNLOAD OUR APP'=>'DESCARGA NUESTRA APP',
	'We Using safe payments'=>'App´u utiliza pagos seguros',
	'SUBSCRIBE FOR THE LATEST NEWS AND GET 10% DISCOUNT ON SPECIAL ITEMS'=>'SUSCRÍBETE A LAS ÚLTIMAS NOTICIAS Y CONSIGUE UN 10% DE DESCUENTO EN ARTÍCULOS ESPECIALES',
	'SUBSCRIBE FOR LATEST UPDATES'=>'SUSCRÍBETE A LAS ÚLTIMAS ACTUALIZACIONES',
	'Sign Up for Our Newsletter'=>'Suscríbete a nuestro boletín',
	'Be the first to learn about our latest trends and get exclusive offers'=>'Sé el primero en conocer nuestras últimas tendencias y recibir ofertas exclusivas.',
	'OK, I agree'=>'Sí, estoy de acuerdo',
	'This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. Review our'=>'Este sitio utiliza cookies. Si continúas navegando por el sitio, estás aceptando nuestro uso de cookies. Revisa nuestro',
	'cookies information'=>'información de cookies',
	'for more details'=>'para más detalles',
	'Enter Your Email Address'=>'Introdue tu dirección de correo electrónico',
	'Quick View'=>'Vista rápida',
	'Categories Text For Home Page'=>'Todo lo que necesitas para Tu Compra Diaria ',
	'WELCOME TO STORE'=>'BIENVENID@ A LA TIENDA',
	'WELCOME TO STORE DETAIL'=>'La Compra de todos los días ahora en tu casa con App´u.',
	'Top Sellings Of the Week Detail'=>'Te recordamos los productos que por su época del año se solicitan más. ',
	'Newest Products Detail'=>'Detalle de los productos recien llegados.',
	'Flash Sale Text'=>'Aprovecha esta oferta semanal.',
	'Super deal of the Month'=>'Súper Oferta del mes',	
	'From our News'=>'Noticias y Eventos',
	'From our News Text'=>'Nuestra actualidad',
	'Read More'=>'Leer más',
	'Days'=>'Días',
	'Hours'=>'Horas',
	'Minutes'=>'Min.',
	'Seconds'=>'Seg.',
	'Countdown Timer'=>'Cuenta atrás',
	'Banner 1'=>'BEBIDAS',
	'Banner 1 Text'=>'Aguas, Refrescos y Zumos, ',
	'Banner 2'=>'LIMPIEZA DEL HOGAR',
	'Banner 2 Text'=>'Tus productos de limpieza en casa',
	'View All Range'=>'Ver todo el rango',
	'Access Your Account Through Your Social Networks'=>'Accede a tu cuenta a través de tu red social',
	'Google'=>'Google',
	'Facebook'=>'Facebook',
	'E-mail'=>'E-mail',
	'DOB'=>'Fecha Nacimiento',
	'Remove'=>'Eliminar',
	'Brands'=>'Marcas',
	'Price Range'=>'Rango de precios',
	'Reviews'=>'Comentarios',
	'Product Added Successfully Thanks.Continue Shopping'=>'Producto añadido con correctamente. Continuar comprando',
	'Add to Wishlist'=>'Añadir a la lista de deseos',
	'Add to Compare'=>'Añadir a comparar',
	'Descriptions'=>'Descripciones',
	'Customer Reviews'=>'Opiniones de usuarios',
	'Please enter your email'=>'Por favor introduce tu correo electrónico',
	'Write Your Review'=>'deja tus comentarios',
	'Rating'=>'Clasificación',
	'Review'=>'Comentario',
	'Write a Review'=>'deja tus comentarios',
	'Thanks For Your Time And Considration For Providing FeedBack For This Product'=>'Gracias por tu tiempo para aportar comentarios para este producto.',
	'In Order To Give FeedBack You Have Must Login First. Thanks'=>'Para aportar comentarios, primero debes iniciar sesión. Gracias.',
	'You Have Already Given The Comment. Thanks'=>'Ya has dejado el comentario. Gracias',
	'Related Products Text'=>'Otros productos que pueden interesarte',
	'Best Sellers'=>'Los más vendidos',
	'APPLY'=>'APLICAR',
	'Demo Store 3654123'=>'08028 Barcelona',
	'Contact us text'=>'Déjanos tu mensaje. Respondemos a todos los correos en un máximo de 24 horas.',
	'CONTACT US'=>'CONTACTO',
	'ADDRESS'=>'DIRECCIÓN',
	'EMAIL ADDRESS'=>'EMAIL',
	'FAX'=>'FAX',
	'COMPARE PRODUCT'=>'COMPARAR PRODUCTO',
	'Thank You'=>'Gracias',
	'You have successfully place your order'=>'Has realizado correctamente tu pedido.',
	'Order page'=>'Página de pedido',
	'Product Added Successfully'=>'Producto añadido correctamente',
	'Categroy'=>'Categoría',
	'Available'=>'Disponible',	
	'Please enter your review'=>'Puedes introducir tu opinión',
	'Product is ready to compare!'=>'¡El producto está listo para comparar!',
	'Current password is invalid'=>'La contraseña actual no es válida.',
	'Please fill all the input fields'=>'Por favor completa todos los campos.',
	'New and confirm password does not match'=>'Nueva y confirmar contraseña no coinciden.',
	'Please enter at least 6 characters'=>'Introduce al menos 6 caracteres.',
	'New Price'=>'Nuevo Precio',
	'Enter Your Zip / Postal Code'=>'Introduce tu Código Postal',
	'Enter Your Phone Number'=>'Introduce tu número de telefóno',
	'NEW CUSTOMER'=>'NUEVO CLIENTE',
	'Product ID'=>'ID de producto',
	'LOGIN'=>'ACCEDER',
	'PRODUCT CATEGORIES'=>'CATEGORÍAS DE PRODUCTOS',
	'NEW ARRIVAL'=>'RECIEN LLEGADOS',
	'Comming Soon'=>'Próximamente',
	'No Record Found!'=>'¡Ningún registro encontrado!',
	'off'=>'apagado',
	'Wishlist'=>'Lista de Deseos',
	'Quick View'=>'Vista rápida',
	'Countdown Timer'=>'Cuenta atrás',
	'Loading'=>'Cargando',
	'OR'=>'ó',
	'or'=>'ó',
	'Or'=>'ó',
	'orders'=>'Pedidos',

	//new labels
	'back_to_top'=>'Subir',
	'modal_text_1' => 'Lorem ipsum dolor sit amet',
	'awesome_5_stars'=>'Impresionante - 5 estrellas',
	'pretty_good_4_stars'=>'Bastante bueno - 4 estrellas',
	'pretty_good_3_stars'=>'Bastante bueno - 3 estrellas',
	'meh_2_stars'=>'Recomendado - 2 estrellas',
	'meh_1_stars'=>'Recomendado - 2 estrellas',
	'twitter'=>'',
	'google'=>'Google',
	'Instagram'=>'Instagram',
	'logo'=>'logo',
	'mail'=>'correo',
	'facebook'=>'facebook',
	'mailchimp_subscribe'=>'Te has suscrito correctamente.',
	'mailchimp_already_subscribe'=>'Ya te has suscrito.',
	'mailchimp_error'=>'¡Uy! Algo salió mal. Por favor, inténtalo de nuevo más tarde.',
	'Orderdetail'=>'Detalle del Pedido',
	'Bank Detail'=>'Datos del Banco',
	'account_name'=>'Nombre de la Cuenta',
	'Bank'=>'Banco',
	'account_number'=>'Número de cuenta',
	'short_code'=>'Código corto',
	'iban'=>'IBAN',
	'swift'=>'Swift',
	'View Order Detail'=>'Ver detalles del pedido',
	'DeliveryboyInfo'=>'Información de Repartidor',
	'DeliveryboyName'=>'Nombre',
	'Contact#'=>'Contacto',
	'Track'=>'Track',
    'Enter Your City'=>'Introduce tu ciudad',
    'Enter Your Company Name'=>'Introduce el nombre de tu empresa',
    'Enter Your Name'=>'Introduce tu nombre',
    'Enter Your Address'=>'Introduce tu dirección',
    'Enter Your Last Name'=>'Introduce tu apellido',
    'Enter Your Email'=>'Introduce tu correo electrónico',

     'Cart Discount'=>'Descuento en la Compra',
     'Cart % Discount'=>'% de Descuento en la Compra',
     'Product Discount'=>'Descuento en Producto',
     'Product % Discount'=>'% de Descuento en Producto'
];
