<div class="col-md-12 table-responsive">
    <table class="table table-fixed" id="tbReviewDT">
        <thead>
        <tr>
            <th>#</th>
            <th>Tipo Operación</th>
            <th>{{ trans('labels.Stock') }}</th>
            <th>{{ trans('labels.Purchase Price') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($inventario as $KVI=> $invent)
            <tr>
                <td>{{$KVI+1}}</td>
                <td>{{$invent->stock_type}}</td>
                <td>
                    @if($invent->stock_type=='out')
                        {{$invent->stock}}
                    @else
                        <input  type="number" min="0.0" step="0.01" id="stock_{{$invent->inventory_ref_id}}" class="form-control" value="{{$invent->stock}}">
                    @endif
                </td>
                <td>
                    @if($invent->stock_type=='out')
                        {{$invent->purchase_price}}
                    @else
                        <input type="number"  min="0.0" step="0.01" id="purchase_{{$invent->inventory_ref_id}}" class="form-control" value="{{$invent->purchase_price}}">
                    @endif
                </td>
                <td>
                    @if($invent->stock_type=='in')
                    <button class="btn btn-sm btn-primary" onclick="updateInventary('{{$invent->inventory_ref_id}}')">salvar</button>

                        <button class="btn btn-sm btn-danger" onclick="deleteInventary('{{$invent->inventory_ref_id}}')">eliminar</button>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>