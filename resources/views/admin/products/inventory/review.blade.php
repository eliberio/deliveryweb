@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{ trans('labels.Inventory') }} <small>{{ trans('labels.Inventory') }}...</small></h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i
                                class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li><a href="{{ URL::to('admin/products/display') }}"><i
                                class="fa fa-database"></i> {{ trans('labels.ListingAllProducts') }}</a></li>
                @if(count($result['products'])> 0 && $result['products'][0]->products_type==1)
                    <li>
                        <a href="{{ URL::to('admin/products/attach/attribute/display/'.$result['products'][0]->products_id) }}">{{ trans('labels.AddOptions') }}</a>
                    </li>
                @endif
                <li class="active">{{ trans('labels.Inventory') }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->

            <!-- /.row -->
            <div class="row">
                <div class="col-md-8">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{ trans('labels.Edit Stock') }} </h3>

                        </div>
                        <div class="box-body">

                            <div class="row">
                                <div class="col-xs-12">
                                    @if (count($errors) > 0)
                                        @if($errors->any())
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    @endif
                                </div>

                            </div>

                            <div class="row">
                                <!-- Left col -->
                                <div class="col-md-12">
                                    <!-- MAP & BOX PANE -->

                                    <!-- /.box -->
                                    <div class="row">
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                            <!-- USERS LIST -->
                                            <div class="box box-info">
                                                <!-- /.box-header -->
                                                <div class="box-body">


                                                    <div class="form-group">
                                                        <label for="name"
                                                               class="col-sm-2 col-md-4 control-label">{{ trans('labels.Products') }}
                                                            <span style="color:red;">*</span> </label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <select class="form-control field-validate product-type-review">
                                                                <option value="">{{ trans('labels.Choose Product') }}</option>
                                                                @foreach ($result['products'] as $pro)
                                                                    <option @if($pro->products_id==request()->get('producto')) selected="selected"
                                                                            @endif value="{{$pro->products_id}}">{{$pro->products_name}}</option>
                                                                @endforeach
                                                            </select><span class="help-block"
                                                                           style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                                            {{ trans('labels.Product Type Text') }}.</span>
                                                        </div>
                                                    </div>
                                                    <div id="attribute" style="display:none">

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-4 control-label">
                                                            {{ trans('labels.Current Stock') }}
                                                        </label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <p id="current_stocks" style="width:100%">0</p><br>

                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-4 control-label">
                                                            {{ trans('labels.Total Purchase Price') }}
                                                        </label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <p class="purchase_price_content"
                                                               style="width:100%">@if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                                                                <span id="total_purchases">0</span></p><br>

                                                        </div>
                                                    </div>

                                                    <div class="row justify-content-md-center" id="tblDetInventory">

                                                    </div>
                                                </div>
                                                <!-- /.box-footer -->
                                            </div>
                                            <!--/.box -->
                                        </div>

                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>

                        </div>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>


        </section>
        <!-- /.row -->

        <!-- Main row -->
    </div>


    <script>
        function deleteInventary(_inv) {
            if (confirm('{{__('labels.DeleteFromInventory')}}')) {
                $.ajax({
                    url: '{{ URL::to("admin/products/inventory/review")}}?inv=' + _inv+'&action=2',
                    type: "GET",
                    success: function (res) {
                        location.href='/admin/products/inventory/review'
                    },
                });
            }
        }
        function updateInventary(_inv) {
            if (typeof (Number.parseFloat($("#stock_"+_inv).val())) === 'number' && typeof (Number.parseFloat($("#purchase_"+_inv).val()))==='number') {
            if (confirm('{{__('labels.UpdateInventory')}}')) {
                $.ajax({
                    url: '{{ URL::to("admin/products/inventory/review")}}?inv=' + _inv+'&action=3&stock='+$("#stock_"+_inv).val()+"&purchase="+$("#purchase_"+_inv).val(),
                    type: "GET",
                    success: function (res) {
                        location.href='/admin/products/inventory/review'
                    },
                });
            }
            }else{
                alert('Todos los campos son requeridos');
            }
        }
    </script>
@endsection
